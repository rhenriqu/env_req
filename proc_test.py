import psutil
import subprocess

def is_process_running(process_name):
    """Check if a process is running by its name."""
    for process in psutil.process_iter(['name']):
        if process.info['name'] == process_name:
            return process.pid  # Return process ID if found
    return None

def stream_process_output(pid):
    """Attach to the running process and stream its output."""
    try:
        # Open the process using its PID
        with subprocess.Popen(['tail', '-f', f'/proc/{pid}/fd/1'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True) as proc:
            for line in proc.stdout:
                print(line, end='')  # Print output in real time
    except Exception as e:
        print(f"Error reading output: {e}")

# Example usage
process_name = "makenv.sh"
pid = is_process_running(process_name)

if pid:
    print(f"Process '{process_name}' is running with PID {pid}. Streaming output...")
    stream_process_output(pid)
else:
    print(f"Process '{process_name}' is not running.")
